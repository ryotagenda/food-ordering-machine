import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class FoodOrderingMachine {
    private JPanel root;
    private JButton ramenbutton;
    private JButton udonbutton;
    private JButton sobabutton;
    private JButton gyouzabutton;
    private JButton friedricebutton;
    private JButton currybutton;
    private JTextPane orderedlist;
    private JButton checkoutButton;
    private JLabel total;
    private JLabel sobalabel;
    private JLabel udonlabel;
    private JLabel ramenlabel;
    private JLabel gyouzalabel;
    private JLabel friedricelabel;
    private JLabel currylabel;

    private String currentText;


    private int ramenprice = 800;
    private int udonprice = 600;
    private int sobaprice = 700;
    private int gyouzaprice = 600;
    private int friedriceprice = 800;
    private int curryprice = 550;
    private int allprice = 0;



    public FoodOrderingMachine() {

        ramenbutton.setIcon(new ImageIcon(
                this.getClass().getResource("ラーメン.jpg")
        ));
        udonbutton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));
        sobabutton.setIcon(new ImageIcon(
                this.getClass().getResource("そば.jpg")
        ));
        gyouzabutton.setIcon(new ImageIcon(
                this.getClass().getResource("gyouza.jpg")
        ));
        friedricebutton.setIcon(new ImageIcon(
                this.getClass().getResource("friedrice.jpg")
        ));
        currybutton.setIcon(new ImageIcon(
                this.getClass().getResource("curry.jpg")
        ));






    String defaultText = orderedlist.getText();
    currentText = orderedlist.getText();


        ramenbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order Ramen?", "Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {
                    allprice+= ramenprice;
                    total.setText(allprice + " yen");
                    currentText = orderedlist.getText();
                    System.out.println(currentText);
                    orderedlist.setText(currentText + " Ramen \n");
                }

            }
        }); /////ramenはすべていれかえ

        udonbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order Udon?", "Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {

                    allprice+= udonprice;
                    total.setText(allprice + " yen");
                    currentText = orderedlist.getText();
                    orderedlist.setText(currentText + " Udon \n");

                }


            }
        });

        sobabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {   int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order Soba?", "Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {

                    allprice+= sobaprice;
                    total.setText(allprice + " yen");
                    currentText = orderedlist.getText();
                    orderedlist.setText(currentText + " Soba \n");

                }




            }
        });

        gyouzabutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order gyouza?", "Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {

                    allprice+= gyouzaprice;
                    total.setText(allprice + " yen");
                    currentText = orderedlist.getText();
                    orderedlist.setText(currentText + " Gyuoza \n");

                }


            }
        });

        friedricebutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order friedrice?", "Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {

                    allprice+= friedriceprice;
                    total.setText(allprice + " yen");
                    currentText = orderedlist.getText();
                    orderedlist.setText(currentText + " Fried ride \n");

                }


            }
        });

        currybutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to order curry?", "Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {

                    allprice+= curryprice;
                    total.setText(allprice + " yen");
                    currentText = orderedlist.getText();
                    orderedlist.setText(currentText + " Curry \n");

                }


            }
        });

        checkoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,"Would you like to check out?", "Check out Confirmation",JOptionPane.YES_NO_OPTION);
                if (confirmation == 0)  {
                    if(allprice>0) {
                        JOptionPane.showMessageDialog(null,"Total price is "+allprice+" yen.");
                        allprice =0;
                        total.setText(allprice + " yen");
                        orderedlist.setText(defaultText);
                        currentText = orderedlist.getText();
                    }
                    else{
                        JOptionPane.showMessageDialog(null,"Please order");
                    }



                }


            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }










}
